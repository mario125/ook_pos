part of 'bloc_login_bloc.dart';

@immutable
abstract class BlocLoginEvent {}

class GetUsers extends BlocLoginEvent {}

class LoginUser extends BlocLoginEvent {
  final BuildContext context;
  final ModelUserGet modelUser;

  LoginUser(
    this.context,
    this.modelUser
  );
}
