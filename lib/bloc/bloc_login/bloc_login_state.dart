part of 'bloc_login_bloc.dart';

@immutable
class BlocLoginState {
  final List<ModelUserGet> peoplelist;

  BlocLoginState({required this.peoplelist});

  static BlocLoginState get initialState => new BlocLoginState(
        peoplelist: [],
      );

  BlocLoginState copyWith({
    List<ModelUserGet>? peoplelist,
  }) {
    return BlocLoginState(
      peoplelist: peoplelist ?? this.peoplelist,
    );
  }
}
