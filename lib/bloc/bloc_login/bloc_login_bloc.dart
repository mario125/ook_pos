import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:okk_pos/api/api_user.dart';
import 'package:okk_pos/model/model_user/model_user.dart';
import 'package:okk_pos/utils/UserPreferences.dart';

part 'bloc_login_event.dart';
part 'bloc_login_state.dart';

class BlocLoginBloc extends Bloc<BlocLoginEvent, BlocLoginState> {
  BlocLoginBloc() : super(BlocLoginState.initialState);

  ApiPeople _api = new ApiPeople();
  Map<String, dynamic> _map = Map<String, dynamic>();
  ModelUserGet _modelPeople = new ModelUserGet();
  Map<String, dynamic> _mapJson = new Map<String, dynamic>();
  List<ModelUserGet> _listUsers = [];
  UserPreferences _prefe = UserPreferences();

  @override
  Stream<BlocLoginState> mapEventToState(
    BlocLoginEvent event,
  ) async* {
    if (event is GetUsers) {
      _map = await _api.getUsers();

      print(_map["data"]["responseData"]);

      for (_mapJson in _map["data"]["responseData"]) {
        _modelPeople = ModelUserGet.fromJson(_mapJson);

        _listUsers.add(_modelPeople);
      }

      yield this.state.copyWith(peoplelist: _listUsers);

      _modelPeople.toJson();
    }
    if (event is LoginUser) {
      _prefe.id = event.modelUser.id;
      _prefe.name = event.modelUser.name;
      _prefe.password = event.modelUser.password;
      _prefe.image = event.modelUser.image;
      _prefe.role = event.modelUser.role[0].permissions.toString();

      print("session is ${event.modelUser.name}");

      Navigator.of(event.context).pushNamedAndRemoveUntil('Admin', (Route<dynamic> route) => false);
    }
  }
}
