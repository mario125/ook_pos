import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:okk_pos/api/api_floors.dart';
import 'package:okk_pos/model/model_floors/model_florrs_places.dart';

part 'bloc_floors_event.dart';
part 'bloc_floors_state.dart';

class BlocFloorsBloc extends Bloc<BlocFloorsEvent, BlocFloorsState> {
  BlocFloorsBloc() : super(BlocFloorsState.initialState);
  ApiFloors _api=new ApiFloors();
  Map<String,dynamic> _map = Map<String,dynamic>();
  Map<String, dynamic> _mapJson = new Map<String, dynamic>();
  ModeFloorsPlace _modelFloors = new ModeFloorsPlace(places: []);
  List<ModeFloorsPlace> _listFloors = [];

  @override
  Stream<BlocFloorsState> mapEventToState( BlocFloorsEvent event,) async* {
    if(event is GetFloorsPlaces){

      print("----------------------------------> paso aquoooooooooooo...");
      _map = await _api.getFloorsPlace();

      print(_map["data"]["responseData"]);

      for (_mapJson in _map["data"]["responseData"]) {
        _modelFloors = ModeFloorsPlace.fromJson(_mapJson);

        _listFloors.add(_modelFloors);
      }

      yield this.state.copyWith(floorsList: _listFloors);

     // _modelPeople.toJson();

    }
  }
}
