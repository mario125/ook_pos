part of 'bloc_floors_bloc.dart';

@immutable
class BlocFloorsState {
  final int? intents;
  final bool? process;
  final List<ModeFloorsPlace> floorsList;

  BlocFloorsState({
    this.intents,
    this.process,
    required this.floorsList,
  });
  static BlocFloorsState get initialState => new BlocFloorsState(
        intents: 0,
        process: false,
        floorsList: [],
      );

  BlocFloorsState copyWith({
    int? intents,
    bool? process,
    List<ModeFloorsPlace>? floorsList,
  }) {
    return BlocFloorsState(
        intents: intents ?? this.intents,
        process: process ?? this.process,
        floorsList: floorsList ?? this.floorsList);
  }
}
