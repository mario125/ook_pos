import 'package:flutter/material.dart';

class UtilsColors {
  static const Color primary = Color(0xFF1FCD6C);
  static const Color secundary = Color(0xFF0D1724);
  static const Color tersary = Color(0xFF5C636C);
  static const Color white = Color(0xFFFFFFFF);
  static const Color cris0 = Color(0xFFF2F2F2);

  static const Color info = Color(0xFFA6DFDE);
  static const Color warning = Color(0xFFFFB03C);
  static const Color danger = Color(0xFFFA5656);
  static const Color success = Color(0xFF9BDF47);
}
