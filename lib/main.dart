import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:okk_pos/pages/page_admin.dart';
import 'package:okk_pos/pages/page_floors.dart';
import 'package:okk_pos/pages/page_login.dart';
import 'package:okk_pos/pages/page_splash.dart';
import 'package:okk_pos/utils/UserPreferences.dart';
import 'package:okk_pos/utils/UtilsColors.dart';

import 'bloc/bloc_floors/bloc_floors_bloc.dart';
import 'bloc/bloc_login/bloc_login_bloc.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = new UserPreferences();
  prefs.initPrefs();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_)  => BlocLoginBloc()),
        BlocProvider(create: (_)  => BlocFloorsBloc()),
      ],
      child: GestureDetector(
        onTap: () {},
        child: GetMaterialApp(
          home: PageSplash(),
          theme: ThemeData(
              primaryColor: UtilsColors.primary,
              accentColor: UtilsColors.secundary,
              fontFamily: 'Gelion'),
          routes: {
            'Login': (_) => PageLogin(),
            'Floors': (_) => PageFloors(),
            'Admin': (_) => PageAdmin(),
          },
          debugShowCheckedModeBanner: false,
        ),
      ),
    );
  }
}
