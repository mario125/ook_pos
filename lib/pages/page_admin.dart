import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:okk_pos/widgets/vertical_tabs.dart';

class PageAdmin extends StatefulWidget {
  PageAdmin({Key? key}) : super(key: key);

  @override
  _PageAdminState createState() => _PageAdminState();
}

class _PageAdminState extends State<PageAdmin> {
  List<ButtonsAdmin>  _buttonsAdmin=[];
  late int  _selected=0;
  
  @override
  Widget build(BuildContext context) {
    _buttonsAdmin.add(new ButtonsAdmin(title: "title1", icon: Icons.ac_unit_outlined));
    _buttonsAdmin.add(new ButtonsAdmin(title: "title2", icon: Icons.ac_unit_outlined));
    _buttonsAdmin.add(new ButtonsAdmin(title: "title3", icon: Icons.ac_unit_outlined));
    _buttonsAdmin.add(new ButtonsAdmin(title: "title4", icon: Icons.ac_unit_outlined));

    return MaterialApp(
      title: 'Title',
      home: Scaffold(
        body: SafeArea(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 80,
                child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: _buttonsAdmin.length,
                    itemBuilder: (BuildContext context, int index) => Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: InkResponse(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Icon(
                                  _buttonsAdmin[index].icon,
                                  color: _selected == index ? Colors.red : null,
                                  size: 28,
                                ),
                                Text(_buttonsAdmin[index].title, style: TextStyle(color: _selected == index ? Colors.red : null)),
                              
                              ],
                            ),
                            onTap: () => setState(
                              () {
                                print(_buttonsAdmin[index].title);
                                _selected = index;
                              },
                            ),
                          ),
                        )),
              ),
              Expanded(
                      child: ListView.builder(
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                             
                            },
                            child: new Card(
                              child: new SizedBox(
                                height: 60.0,
                                child: new Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Expanded(
                                      child: Column(
                                        children: [
                                          Row(
                                            children: [
                                              Container(
                                                width: 82,
                                                child: Text(
                                                  " Cod. Entrega ",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 11),
                                                ),
                                              ),
                                              Container(
                                                padding: EdgeInsets.all(0.0),
                                                child: Text(
                                                  ': hghjgj',
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.white30,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Container(
                                                width: 82,
                                                child: Text(
                                                  " Empresa ",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 11),
                                                ),
                                              ),
                                              Container(
                                                padding: EdgeInsets.all(0.0),
                                                child: Text(
                                                  ': h',
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.white30,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Container(
                                                width: 82,
                                                child: Text(
                                                  " Producto ",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 11),
                                                ),
                                              ),
                                              Flexible(
                                                child: Container(
                                                  padding: EdgeInsets.all(0.0),
                                                  child: Text(
                                                    ': dato',
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: new TextStyle(
                                                      fontSize: 12,
                                                      color: Colors.white30,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.center,
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 1.0),
                                      child: Icon(
                                        Icons.delete,
                                        size: 30,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                        itemCount: 1,
                      ),
                    ),

              Expanded(
                child: Container(
                  child: VerticalTabs(
                    tabsWidth: 120,
                    tabs: <Tab>[
                      Tab(child: Text('Flutter'), icon: Icon(Icons.phone)),
                      Tab(child: Text('Dart')),
                      Tab(
                        child: Container(
                          margin: EdgeInsets.only(
                            left: 1,
                            top: 10,
                            right: 5,
                            bottom: 10,
                          ),
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.favorite),
                              SizedBox(
                                width: 1,
                                height: 10,
                              ),
                              Text('Javascript'),
                            ],
                          ),
                        ),
                      ),
                      Tab(child: Text('NodeJS')),
                      Tab(child: Text('PHP')),
                      Tab(child: Text('HTML 5')),
                      Tab(child: Text('CSS 3')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 5')),
                      Tab(child: Text('CSS 125')),
                    ],
                    contents: <Widget>[
                      Container(
                        color: Colors.black12,
                        child: ListView(
                          children: [
                            GridView.builder(
                              shrinkWrap: true,
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2, // cantidad x fila
                              ),
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: 10,
                              itemBuilder: (_, index2) => InkWell(
                                focusColor: Colors.transparent,
                                hoverColor: Colors.transparent,
                                enableFeedback: false,
                                onTap: () => {print("mm")},
                                child: Container(
                                  margin: const EdgeInsets.all(5.0),
                                  height: 340,
                                  width: 390,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        height: 50,
                                        width: 50,
                                        child: SvgPicture.asset(
                                            "assets/svg/escalera.svg"),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.all(5.0),
                                        child: Text(
                                          "-2",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Icon(Icons.payment),
                                          ),
                                          Expanded(
                                            child: Icon(Icons.payment),
                                          ),
                                          Expanded(
                                            child: Icon(Icons.payment),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          topRight: Radius.circular(10),
                                          bottomLeft: Radius.circular(10),
                                          bottomRight: Radius.circular(10)),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black26,
                                          blurRadius: 15,
                                          spreadRadius: 0.5,
                                          offset: Offset(1, 4),
                                        )
                                      ]),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.black12,
                        child: ListView(
                          children: [
                            GridView.builder(
                              shrinkWrap: true,
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2, // cantidad x fila
                              ),
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: 10,
                              itemBuilder: (_, index2) => InkWell(
                                focusColor: Colors.transparent,
                                hoverColor: Colors.transparent,
                                enableFeedback: false,
                                onTap: () => {print("mm")},
                                child: Container(
                                  margin: const EdgeInsets.all(5.0),
                                  height: 340,
                                  width: 390,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        height: 50,
                                        width: 50,
                                        child: SvgPicture.asset(
                                            "assets/svg/escalera.svg"),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.all(5.0),
                                        child: Text(
                                          "-2",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Icon(Icons.payment),
                                          ),
                                          Expanded(
                                            child: Icon(Icons.payment),
                                          ),
                                          Expanded(
                                            child: Icon(Icons.payment),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          topRight: Radius.circular(10),
                                          bottomLeft: Radius.circular(10),
                                          bottomRight: Radius.circular(10)),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black26,
                                          blurRadius: 15,
                                          spreadRadius: 0.5,
                                          offset: Offset(1, 4),
                                        )
                                      ]),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.black12,
                        child: ListView(
                          children: [
                            GridView.builder(
                              shrinkWrap: true,
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2, // cantidad x fila
                              ),
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: 10,
                              itemBuilder: (_, index2) => InkWell(
                                focusColor: Colors.transparent,
                                hoverColor: Colors.transparent,
                                enableFeedback: false,
                                onTap: () => {print("mm")},
                                child: Container(
                                  margin: const EdgeInsets.all(5.0),
                                  height: 340,
                                  width: 390,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        height: 50,
                                        width: 50,
                                        child: SvgPicture.asset(
                                            "assets/svg/escalera.svg"),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.all(5.0),
                                        child: Text(
                                          "-2",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Icon(Icons.payment),
                                          ),
                                          Expanded(
                                            child: Icon(Icons.payment),
                                          ),
                                          Expanded(
                                            child: Icon(Icons.payment),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          topRight: Radius.circular(10),
                                          bottomLeft: Radius.circular(10),
                                          bottomRight: Radius.circular(10)),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black26,
                                          blurRadius: 15,
                                          spreadRadius: 0.5,
                                          offset: Offset(1, 4),
                                        )
                                      ]),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.black12,
                        child: ListView(
                          children: [
                            GridView.builder(
                              shrinkWrap: true,
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2, // cantidad x fila
                              ),
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: 10,
                              itemBuilder: (_, index2) => InkWell(
                                focusColor: Colors.transparent,
                                hoverColor: Colors.transparent,
                                enableFeedback: false,
                                onTap: () => {print("mm")},
                                child: Container(
                                  margin: const EdgeInsets.all(5.0),
                                  height: 340,
                                  width: 390,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        height: 50,
                                        width: 50,
                                        child: SvgPicture.asset(
                                            "assets/svg/escalera.svg"),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.all(5.0),
                                        child: Text(
                                          "-2",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Icon(Icons.payment),
                                          ),
                                          Expanded(
                                            child: Icon(Icons.payment),
                                          ),
                                          Expanded(
                                            child: Icon(Icons.payment),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          topRight: Radius.circular(10),
                                          bottomLeft: Radius.circular(10),
                                          bottomRight: Radius.circular(10)),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black26,
                                          blurRadius: 15,
                                          spreadRadius: 0.5,
                                          offset: Offset(1, 4),
                                        )
                                      ]),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.black12,
                        child: ListView(
                          children: [
                            GridView.builder(
                              shrinkWrap: true,
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2, // cantidad x fila
                              ),
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: 50,
                              itemBuilder: (_, index2) => InkWell(
                                focusColor: Colors.transparent,
                                hoverColor: Colors.transparent,
                                enableFeedback: false,
                                onTap: () => {print("mm")},
                                child: Container(
                                  margin: const EdgeInsets.all(5.0),
                                  height: 340,
                                  width: 390,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        height: 50,
                                        width: 50,
                                        child: SvgPicture.asset(
                                            "assets/svg/escalera.svg"),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.all(5.0),
                                        child: Text(
                                          "-2",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Icon(Icons.payment),
                                          ),
                                          Expanded(
                                            child: Icon(Icons.payment),
                                          ),
                                          Expanded(
                                            child: Icon(Icons.payment),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          topRight: Radius.circular(10),
                                          bottomLeft: Radius.circular(10),
                                          bottomRight: Radius.circular(10)),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black26,
                                          blurRadius: 15,
                                          spreadRadius: 0.5,
                                          offset: Offset(1, 4),
                                        )
                                      ]),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      tabsContent('HTML 5'),
                      Container(
                        color: Colors.black12,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: 10,
                          itemExtent: 100,
                          itemBuilder: (context, index) {
                            return Container(
                              margin: EdgeInsets.all(10),
                              color: Colors.white30,
                            );
                          },
                        ),
                      ),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML f'),
                      tabsContent('HTML 125'),
                    ],
                  ),
                ),
              ),
            ],
          ),

        ),
      ),
    );
  }

  Widget tabsContent(String caption, [String description = '']) {
    return Container(
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(20),
      color: Colors.black12,
      child: Column(
        children: <Widget>[
          Text(
            caption,
            style: TextStyle(fontSize: 25),
          ),
          Divider(
            height: 20,
            color: Colors.black45,
          ),
          Text(
            description,
            style: TextStyle(fontSize: 15, color: Colors.black87),
          ),
        ],
      ),
    );
  }
}

class ButtonsAdmin {
  String title;
  IconData icon;
  ButtonsAdmin({required this.title, required this.icon});
}
