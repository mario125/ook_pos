import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_svg/svg.dart';
import 'package:okk_pos/bloc/bloc_login/bloc_login_bloc.dart';
import 'package:okk_pos/model/model_user/model_user.dart';
import 'package:okk_pos/utils/UtilsColors.dart';
import 'package:passcode_screen/circle.dart';
import 'package:passcode_screen/keyboard.dart';
import 'package:passcode_screen/passcode_screen.dart';

class PageLogin extends StatefulWidget {
  PageLogin({Key? key}) : super(key: key);

  @override
  _PageLoginState createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {
  BlocLoginBloc _bloc = BlocLoginBloc();

  @override
  void initState() {
    _bloc.add(GetUsers());
    super.initState();
  }

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          leading: BackButton(
            color: Colors.black,
          ),
        ),
        body: BlocBuilder<BlocLoginBloc, BlocLoginState>(
          builder: (_, state) {
            return Stack(
              alignment: AlignmentDirectional.topStart,
              children: [
                ListView(
                  physics: BouncingScrollPhysics(),
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: ListUsersCard(
                        items: state.peoplelist,
                      ),
                    ),
                    SizedBox(height: 100)
                  ],
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}

class ListUsersCard extends StatefulWidget {
  final List<ModelUserGet> items;

  const ListUsersCard({required this.items});

  @override
  _ListUsersCardState createState() => _ListUsersCardState();
}

class _ListUsersCardState extends State<ListUsersCard> {
  final StreamController<bool> _verificationNotifier =  StreamController<bool>.broadcast();

  bool isAuthenticated = false;
  void sendFloors() {
    Navigator.of(context).pushNamedAndRemoveUntil('Admin', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    

    return Wrap(
      direction: Axis.horizontal,
      alignment: WrapAlignment.start,
      spacing: 15,
      runSpacing: 15,
      children: List.generate(
        widget.items.length,
        (index) => 
        InkWell(
          onTap: () => _showLockScreen(
            context,
            modelUser: widget.items[index],
            opaque: false,
            circleUIConfig: CircleUIConfig(
              borderColor: UtilsColors.primary,
              fillColor: UtilsColors.primary,
              circleSize: 30,
            ),
            keyboardUIConfig: KeyboardUIConfig(
              digitBorderWidth: 2,
              primaryColor: UtilsColors.primary,
            ),
            cancelButton: Text(
              'Cancel',
              style: const TextStyle(fontSize: 16, color: Colors.white),
              semanticsLabel: 'Cancel',
            ),
            // cancelButton: Icon(
            //   Icons.arrow_back,
            //   color: Colors.blue,
            // )
          ),
          child: Container(
            height: 120,
            width: 170,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 50,
                  width: 50,
                  child: SvgPicture.asset(widget.items[index].image),
                ),
                Padding(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                    widget.items[index].name,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15),
                  ),
                )
              ],
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: Color(colorice()),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 2,
                    spreadRadius: 0.5,
                    offset: Offset(1, 4),
                  )
                ]),
          ),
        ),
      ),
    );
  }

  _showLockScreen(
    BuildContext context, {
    required ModelUserGet modelUser,
    required bool opaque,
    CircleUIConfig? circleUIConfig,
    KeyboardUIConfig? keyboardUIConfig,
    required Widget cancelButton,
    List<String>? digits,
  }) {
    Navigator.push(
        context,
        PageRouteBuilder(
          opaque: opaque,
          pageBuilder: (context, animation, secondaryAnimation) =>
              PasscodeScreen(
            title: Text(
              'Ingrese la contraseña',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white, fontSize: 28),
            ),
            circleUIConfig: circleUIConfig,
            keyboardUIConfig: keyboardUIConfig,
            passwordEnteredCallback: (_) {
              _onPasscodeEntered(_, modelUser);
            },
            cancelButton: cancelButton,
            deleteButton: Text(
              'Eliminar',
              style: const TextStyle(fontSize: 16, color: Colors.white),
              semanticsLabel: 'Eliminar',
            ),
            shouldTriggerVerification: _verificationNotifier.stream,
            backgroundColor: Colors.black.withOpacity(0.8),
            cancelCallback: _onPasscodeCancelled,
            digits: digits,
            passwordDigits: 4,
            bottomWidget: null,
          ),
        ));
  }

  _onPasscodeEntered(String code, ModelUserGet model) {
    print(code);
    print("----${model.name}----------${model.password}-----------------");
    print(model.name);
    final _bloc = BlocProvider.of<BlocLoginBloc>(context);

    bool isValid = model.password == code;
    _verificationNotifier.add(isValid);
    if (isValid) {
      _bloc.add(LoginUser(context, model));     
    }
  }

  _onPasscodeCancelled() {
    Navigator.maybePop(context);
  }

  @override
  void dispose() {
    _verificationNotifier.close();
    super.dispose();
  }
}

int random(min, max) {
  var rn = new Random();
  return min + rn.nextInt(max - min);
}

int colorice() {
  int rn = random(1, 10);
  int color;
  switch (rn) {
    case 1:
      color = 0XFFF6F6F6;
      break;
    case 2:
      color = 0XFFFCF5EB;
      break;
    case 3:
      color = 0XFFFEEFEF;
      break;
    case 4:
      color = 0XFFEDF3FE;
      break;
    case 5:
      color = 0XFFF6F6F6;
      break;
    case 6:
      color = 0XFFF6F6F6;
      break;
    case 7:
      color = 0XFFFCF5EB;
      break;
    case 8:
      color = 0XFFFEEFEF;
      break;
    case 9:
      color = 0XFFEDF3FE;
      break;
    case 10:
      color = 0XFFF6F6F6;
      break;
    default:
      color = 0XFFF6F6F6;
  }
  return color;
}
