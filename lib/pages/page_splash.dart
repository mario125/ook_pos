import 'dart:async';

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/route_manager.dart';
import 'package:okk_pos/pages/page_floors.dart';

class PageSplash extends StatefulWidget {
  PageSplash({Key? key}) : super(key: key);

  @override
  _PageSplashState createState() => _PageSplashState();
}

class _PageSplashState extends State<PageSplash> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 5), () => sendLogin());
  }
  void sendLogin() { 
    //Get.off(()=>PageFloors());
    Navigator.pushReplacementNamed(context,'Login');    
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Container(
            child: Center(
              child: ZoomIn(
                delay: Duration(milliseconds: 1500),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: 170,
                      width: 170,
                      child: SvgPicture.asset("assets/svg/logo_vite.svg"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Vite",
                      style: TextStyle(
                        fontSize: 45,
                        fontFamily: 'Cocon',
                        letterSpacing: 2,
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
