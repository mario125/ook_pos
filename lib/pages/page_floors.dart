import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:okk_pos/bloc/bloc_floors/bloc_floors_bloc.dart';
import 'package:okk_pos/utils/UserPreferences.dart';
import 'package:okk_pos/utils/UtilsColors.dart';
import 'package:scrollable_list_tabview/scrollable_list_tabview.dart';

class PageFloors extends StatefulWidget {
  PageFloors({Key? key}) : super(key: key);

  @override
  _PageFloorsState createState() => _PageFloorsState();
}

class _PageFloorsState extends State<PageFloors> {
  UserPreferences _pre = new UserPreferences();
  BlocFloorsBloc _bloc = new BlocFloorsBloc();

  @override
  void initState() {
    _bloc.add(GetFloorsPlaces());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _bloc,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          title: Center(
            child: Text(
              "${_pre.name}",
              style: TextStyle(color: UtilsColors.secundary),
            ),
          ),
          backgroundColor: Colors.white,
        ),
        body: BlocBuilder<BlocFloorsBloc, BlocFloorsState>(
          builder: (_, state) {
            return ScrollableListTabView(
              tabHeight: 48,
              bodyAnimationDuration: const Duration(milliseconds: 150),
              tabAnimationCurve: Curves.easeOut,
              tabAnimationDuration: const Duration(milliseconds: 200),
              tabs: List.generate(
                state.floorsList.length,
                (index) => ScrollableListTab(
                  tab: ListTab(
                    activeBackgroundColor: UtilsColors.primary,
                    borderColor: UtilsColors.primary,
                    label: Text("${state.floorsList[index].name}"),
                    icon: Container(
                      height: 80,
                      width: 30,
                      child: SvgPicture.asset(
                        "assets/svg/escalera.svg",
                      ),
                    ),
                  ),
                  body: GridView.builder(
                    shrinkWrap: true,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2, // cantidad x fila
                    ),
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: state.floorsList[index].places.length,
                    itemBuilder: (_, index2) => InkWell(
                      focusColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      enableFeedback: false,
                      onTap: () => {print("${index}")},
                      child: Container(
                        margin: const EdgeInsets.all(20.0),
                        height: 120,
                        width: 170,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              height: 50,
                              width: 50,
                              child:
                                  SvgPicture.asset("assets/svg/escalera.svg"),
                            ),
                            Padding(
                              padding: EdgeInsets.all(5.0),
                              child: Text(
                                "${state.floorsList[index].places[index2].name}",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Row(
                              children:  <Widget>[
                                Expanded(
                                  child: (state.floorsList[index].places[index2].print==null)?Container(): Icon(Icons.payment) ,
                                ),
                                Expanded(
                                  child: Icon(Icons.payment),
                                ),
                                Expanded(
                                  child: Icon(Icons.payment),
                                ),
                              ],
                            )
                          ],
                        ),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                topRight: Radius.circular(10),
                                bottomLeft: Radius.circular(10),
                                bottomRight: Radius.circular(10)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black26,
                                blurRadius: 15,
                                spreadRadius: 0.5,
                                offset: Offset(1, 4),
                              )
                            ]),
                      ),
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
