

import 'package:dio/dio.dart';
import 'api_utils.dart' show ApiUtils;

class ApiFloors {
  Future<Map<String, dynamic>> getFloorsPlace() async {
    Map<String, dynamic> _map = new Map<String, dynamic>();

    Response response;

  

    try {
      response = await ApiUtils.dio.get("floors_places/getFloors_Places");
      print("json-->>= ${response.statusCode}");
      if (response.statusCode == 200) {
        _map["code"] = response.statusCode;
        _map["data"] = response.data;
        return _map;
      }
      return _map;
    } on DioError catch (e) {
      
      print("jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj ${e.error}");
      print("jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj ${e.response}");
      print("jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj ${e.type}");
      print("jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj ${e.hashCode}");
      if(e.response ==null){
        _map["code"] = 0;
        _map["data"] = "Sin conexión a internet.";

        return _map;
      } else {
        _map["code"] = 0;
        _map["data"] = "Conexion fallida";

        return _map;
      }
    }
  }
}
