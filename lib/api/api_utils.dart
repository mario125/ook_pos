import 'package:dio/dio.dart';

class ApiUtils {
  
  static const String URL_API = "http://192.168.1.29/okk_api/public/v1/";

  static const String URL_SOCKET = "-";

  static BaseOptions options = new BaseOptions(
    baseUrl: URL_API,
    connectTimeout: 250000,
    receiveTimeout: 250000,
    contentType: "application/json;charset=UTF-8",
  );

  static Dio dio = new Dio(options);

}
