// To parse this JSON data, do
//
//     final modeFloorsPlace = modeFloorsPlaceFromJson(jsonString);

import 'dart:convert';

ModeFloorsPlace modeFloorsPlaceFromJson(String str) => ModeFloorsPlace.fromJson(json.decode(str));

String modeFloorsPlaceToJson(ModeFloorsPlace data) => json.encode(data.toJson());

class ModeFloorsPlace {
    ModeFloorsPlace({
        this.id,
        this.name,
        this.image,
        required this.places,
    });

    dynamic id;
    dynamic name;
    dynamic image;
    List<Place> places;

    factory ModeFloorsPlace.fromJson(Map<String, dynamic> json) => ModeFloorsPlace(
        id: json["id"],
        name: json["name"],
        image: json["image"],
        places: List<Place>.from(json["places"].map((x) => Place.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "image": image,
        "places": List<dynamic>.from(places.map((x) => x.toJson())),
    };
}

class Place {
    Place({
        this.id,
        this.name,
        this.idFloors,
        this.shared,
        this.print,
        this.preAccount,
    });

    dynamic id;
    dynamic name;
    dynamic idFloors;
    dynamic shared;
    dynamic print;
    dynamic preAccount;

    factory Place.fromJson(Map<String, dynamic> json) => Place(
        id: json["id"],
        name: json["name"],
        idFloors: json["id_floors"],
        shared: json["shared"],
        print: json["print"],
        preAccount: json["pre_account"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "id_floors": idFloors,
        "shared": shared,
        "print": print,
        "pre_account": preAccount,
    };
}
