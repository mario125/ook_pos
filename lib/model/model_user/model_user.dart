// To parse this JSON data, do
//
//     final modelUserGet = modelUserGetFromJson(jsonString);

import 'dart:convert';

List<ModelUserGet> modelUserGetFromJson(String str) => List<ModelUserGet>.from(
    json.decode(str).map((x) => ModelUserGet.fromJson(x)));

String modelUserGetToJson(List<ModelUserGet> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ModelUserGet {
  ModelUserGet({
    this.id,
    this.name,
    this.password,
    this.idRole,
    this.visible,
    this.image,
    this.dateCreate,
    this.dateEdit,
    this.role,
  });

  dynamic id;
  dynamic name;
  dynamic password;
  dynamic idRole;
  dynamic visible;
  dynamic image;
  dynamic dateCreate;
  dynamic dateEdit;
  dynamic role;

  factory ModelUserGet.fromJson(Map<String, dynamic> json) => ModelUserGet(
        id: json["id"],
        name: json["name"],
        password: json["password"],
        idRole: json["id_role"],
        visible: json["visible"],
        image: json["image"],
        dateCreate: DateTime.parse(json["date_create"]),
        dateEdit: DateTime.parse(json["date_edit"]),
        role: List<Role>.from(json["role"].map((x) => Role.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "password": password,
        "id_role": idRole,
        "visible": visible,
        "image": image,
        "date_create": dateCreate.toIso8601String(),
        "date_edit": dateEdit.toIso8601String(),
        "role": List<dynamic>.from(role.map((x) => x.toJson())),
      };
}

class Role {
  Role({
    this.id,
    this.name,
    this.permissions,
  });

  dynamic id;
  dynamic name;
  dynamic permissions;

  factory Role.fromJson(Map<String, dynamic> json) => Role(
        id: json["id"],
        name: json["name"],
        permissions: json["permissions"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "permissions": permissions,
      };
}
