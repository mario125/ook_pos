// To parse this JSON data, do
//
//     final rolePermissions = rolePermissionsFromJson(jsonString);

import 'dart:convert';

RolePermissions rolePermissionsFromJson(String str) => RolePermissions.fromJson(json.decode(str));

String rolePermissionsToJson(RolePermissions data) => json.encode(data.toJson());

class RolePermissions {
    RolePermissions({
        required this.routes,
    });

    List<Route> routes;

    factory RolePermissions.fromJson(Map<String, dynamic> json) => RolePermissions(
        routes: List<Route>.from(json["routes"].map((x) => Route.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "routes": List<dynamic>.from(routes.map((x) => x.toJson())),
    };
}

class Route {
    Route({
        required this.icon,
        required this.page,
        required this.title,
    });

    String icon;
    String page;
    String title;

    factory Route.fromJson(Map<String, dynamic> json) => Route(
        icon: json["icon"],
        page: json["page"],
        title: json["title"],
    );

    Map<String, dynamic> toJson() => {
        "icon": icon,
        "page": page,
        "title": title,
    };
}
